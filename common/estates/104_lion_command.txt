
estate_lion_command = {
	icon = 26
	color = { 66 0 116 }
	
	trigger = {
		tag = R62
	}
	
	country_modifier_happy = {
		global_manpower = 10	# +10k flat manpower
		war_exhaustion = -0.05	# -0.05 monthly war exhaustion
		general_cost = -0.15	# -15% general cost
	}
	
	country_modifier_neutral = {
		global_manpower = 10	# +10k flat manpower
	}
	
	country_modifier_angry = {
		war_exhaustion = 0.05	# +0.05 monthly war exhaustion
	}
	
	land_ownership_modifier = {
		lion_command_loyalty_modifier = 0.2	# +20% loyalty equilibrium, scale with land ownership
	}
	
	province_independence_weight = {
		factor = 26
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}
	
	base_influence = 10.0
	
	influence_modifier = {
		desc = "Influence Modifier: "
		trigger = {
			#<triggers>
		}
		influence = 10.0
	}
	
	loyalty_modifier = {
		desc = "Legitimacy below 40: "
		trigger = {
			NOT = { legitimacy = 40 }
		}
		loyalty = -20.0
	}
	loyalty_modifier = {
		desc = "Legitimacy below 50: "
		trigger = {
			legitimacy = 40
			NOT = { legitimacy = 50 }
		}
		loyalty = -15.0
	}
	loyalty_modifier = {
		desc = "Legitimacy below 60: "
		trigger = {
			legitimacy = 50
			NOT = { legitimacy = 60 }
		}
		loyalty = -10.0
	}
	loyalty_modifier = {
		desc = "Legitimacy below 70: "
		trigger = {
			legitimacy = 60
			NOT = { legitimacy = 70 }
		}
		loyalty = -5.0
	}
	loyalty_modifier = {
		desc = "Legitimacy over 90: "
		trigger = {
			legitimacy = 90
		}
		loyalty = 5.0
	}
	
	contributes_to_curia_treasury = no
	
	privileges = {
		estate_lion_command_land_rights
		estate_lion_command_oratory_schools
		estate_lion_command_x
		estate_lion_command_war_room_funds
		estate_lion_command_exaltations_from_leadership
		estate_lion_command_management
		estate_lion_command_ninyu_kikun_officers
	}
	
	agendas = {
		estate_lion_command_campaign_bianfang
		estate_lion_command_campaign_dhenbasana
		estate_lion_command_campaign_baihon_xinh
		estate_lion_command_campaign_xianjie
		estate_lion_command_campaign_tianlou
		estate_lion_command_campaign_jiantsiang
		estate_lion_command_campaign_tree_of_stone
		
		estate_lion_command_hire_advisor
		estate_lion_command_fire_advisor
		estate_lion_command_retake_core
		estate_lion_command_recover_abysmal_prestige
		estate_lion_command_improve_prestige
	}
	
	influence_from_dev_modifier = 1.0
}