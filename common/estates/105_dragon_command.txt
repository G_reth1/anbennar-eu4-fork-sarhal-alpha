
estate_dragon_command = {
	icon = 27
	color = { 246 0 0 }
	
	trigger = {
		OR = {
			tag = R69
			has_country_flag = dragon_command_founded_flag
		}
	}
	
	country_modifier_happy = {
		global_manpower = 10		# +10k flat manpower
		female_advisor_chance = 0.5	# +50% female advisor chance
		advisor_pool = 1			# +1 possible advisor
		artillery_power = 0.10		# +10% artillery power
	}
	
	country_modifier_neutral = {
		global_manpower = 10	# +10k flat manpower
	}
	
	country_modifier_angry = {
		artillery_power = -0.10	# -10% artillery power
	}
	
	land_ownership_modifier = {
		dragon_command_loyalty_modifier = 0.2	# +20% loyalty equilibrium, scale with land ownership
	}
	
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}
	
	base_influence = 10.0
	
	influence_modifier = {
		desc = "Influence Modifier: "
		trigger = {
			#<triggers>
		}
		influence = 10.0
	}
	
	loyalty_modifier = {
		desc = "Legitimacy below 40: "
		trigger = {
			NOT = { legitimacy = 40 }
		}
		loyalty = -20.0
	}
	loyalty_modifier = {
		desc = "Legitimacy below 50: "
		trigger = {
			legitimacy = 40
			NOT = { legitimacy = 50 }
		}
		loyalty = -15.0
	}
	loyalty_modifier = {
		desc = "Legitimacy below 60: "
		trigger = {
			legitimacy = 50
			NOT = { legitimacy = 60 }
		}
		loyalty = -10.0
	}
	loyalty_modifier = {
		desc = "Legitimacy below 70: "
		trigger = {
			legitimacy = 60
			NOT = { legitimacy = 70 }
		}
		loyalty = -5.0
	}
	loyalty_modifier = {
		desc = "Legitimacy over 90: "
		trigger = {
			legitimacy = 90
		}
		loyalty = 5.0
	}
	
	contributes_to_curia_treasury = no
	
	privileges = {
		estate_dragon_command_land_rights
		estate_dragon_command_x
		estate_dragon_command_management
		estate_dragon_command_ninyu_kikun_sunyanin
	}
	
	agendas = {
		# estate_dragon_command_campaign_shamakhad
	}
	
	influence_from_dev_modifier = 1.0
}