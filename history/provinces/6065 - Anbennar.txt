
# No previous file for Banten
owner = L61
controller = L61
add_core = L61
culture = gataw
religion = melak_temelikh

hre = no

base_tax = 7
base_production = 7
base_manpower = 5

trade_goods = spices

center_of_trade = 1

capital = ""

is_city = yes