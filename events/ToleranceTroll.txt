namespace = troll_tolerance_events

# Event organizer for the troll tolerance events - should be a huge improvement to performance - having 1 overall check for having the minority instead of one for EACH new event that gets added
country_event = {
	id = troll_tolerance_events.0
	title = race_setup_events.1.d
	desc = race_setup_events.1.d
	picture = BORDER_TENSION_eventPicture

	is_triggered_only = yes
	hidden = yes

	trigger = {
		NOT = { culture_group = giantkind }
		any_owned_province = {
			has_troll_minority_trigger = yes
		}
	}
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 5
			has_country_modifier = racial_focus_troll
		}
	}

	option = {
		# random_list = {
			# # 
			# 1 = {
				# trigger = {
					
				# }
				# modifier = {
					
				# }
				# country_event = {
					# #id = 
				# }
			# }
		
		# }
	}
}